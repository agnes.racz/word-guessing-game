package org.fasttrackit;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static final String GREEN = "\u001B[32m";
    public static final String RED = "\u001B[31m";
    public static final String CYAN = "\u001B[46m";
    public static final String RESET = "\u001B[0m";

    public static void main(String[] args) {

        String[] category = chooseCategory();
        String word = randomStringFromArr(category);

        System.out.print("The word has " + word.length() + " letters.");
        Scanner text = new Scanner(System.in);
        String guess = "";

        while (!guess.equals(word)) {
            System.out.print("\nPlease enter your guess: ");

            guess = text.nextLine();
            guess = checkLength(guess, word);
            guess = guess.toUpperCase();
            checkEveryChar(guess, word);
        }

        System.out.println();
        System.out.printf("%90s%n", CYAN + "******** CONGRATS! Your word is correct! ********" + RESET);
    }

    public static void checkEveryChar(String guess1, String original) {

        for (int i = 0; i < guess1.length(); i++) {
            String cGuess = guess1.substring(i, i + 1);
            int count = 0;

            if (cGuess.equals(original.substring(i, i + 1))) {
                System.out.print(GREEN + cGuess + RESET);
                count++;
            } else {
                for (int j = 0; j < original.length(); j++) {
                    String cWord = original.substring(j, j + 1);

                    if (cGuess.equals(cWord)) {

                        System.out.print(RED + cGuess + RESET);
                        count++;
                        break;
                    }
                }
            }

            if (count == 0) System.out.print(cGuess);
        }
    }

    public static String checkLength(String guess1, String original) {

        Scanner txt = new Scanner(System.in);

        while (guess1.length() > original.length()) {
            System.out.print("The word is too long. Please try again: ");
            guess1 = txt.nextLine();
        }

        return guess1;
    }

    public static String randomStringFromArr(String[] arr) {

        int randIdx = ThreadLocalRandom.current().nextInt(arr.length);

        return arr[randIdx];
    }

    public static String[] chooseCategory() {

        String[] family = {"DAUGHTER", "HUSBAND", "SISTER", "BROTHER", "COUSIN", "MOTHER", "FATHER"};
        String[] drinks = {"COFFEE", "MILK", "WATER", "JUICE", "COCKTAIL"};
        String[] animals = {"DOLPHIN", "WHALE", "MANATEE", "ALLIGATOR", "SNAKE", "GORILLA"};

        System.out.println();
        System.out.printf("%90s%n", CYAN + "Welcome to Agnes's Word Guessing Game!" + RESET);
        System.out.println("\nTry to guess a word from a chosen category. I'll help you by coloring your letters:\n\tWHITE - the letter is not in the word" + RED + "\n\tRED" + RESET + " - the letter is in the word, but on bad location" + GREEN + "\n\tGREEN" + RESET + " - the letter is at the correct location");
        System.out.println("Please choose your category:\n\t\t1) Family\n\t\t2) Drinks\n\t\t3) Animals ");
        System.out.print("Category number: ");

        Scanner numb = new Scanner(System.in);
        int category = numb.nextInt();
        String[] array = new String[family.length];

        switch (category) {
            case 1 -> array = family;
            case 2 -> array = drinks;
            case 3 -> array = animals;
            default -> System.out.println("Input incorrect!");
        }

        return array;
    }

}
